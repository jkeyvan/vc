from flask import Flask, render_template,request,redirect,url_for
from flask_sse import sse
rooms={}
peers=[]
data=""
app = Flask(__name__)
app.config["REDIS_URL"] = "redis://localhost"
app.register_blueprint(sse, url_prefix='/stream')

@app.route('/caller')
def index():
    return render_template("index.html")


@app.route('/makeroom/<id>',methods=['POST'])
def makeroom(id):
    channel = request.data
    room = id
    rooms[room]=channel;
    print "room "+str(id) +"created"
    # sse.publish({"message":" room %s created ! "%(id)},type="makeRoomCallee")
    return "room "+str(id) +"created"

@app.route('/offer/<room>',methods=['POST'])
def offer(room):
    data=request.data
    f=1
    for key, value in rooms.iteritems():
        if int(key)==int(room):
            sse.publish({"offer":request.data},type="offer")
            f=0;
    if f==1: 
        print "notfound"
        return "not found such a room"
    print "caller sdp :",data
    return render_template("room.html")

@app.route('/answer',methods=['POST'])
def answer():
    data=request.data
    print "callee sdp :",data
    sse.publish({"answer":request.data},type="answer")
    #todo send to  the channel of caller
    return render_template("room.html")

@app.route('/caller',methods=['GET'])
def caller():
    return render_template("index.html")

@app.route('/callee',methods=['GET'])
def callee():
    return render_template("callee.html")
@app.route('/stream')
def stream():
    sse.publish({"answer":"request.data"},type="answer")
    return render_template("callee.html")

@app.route('/signinPage')
def signinPage():
    return render_template("signin.html")

@app.route('/signin', methods=['POST'])
def signin():
    signinForm = request.form
    username = signinForm['u']
    password= signinForm['p']
    # todo validate 
    return redirect(url_for('home'))

@app.route('/signupPage')
def signupPage():
    return render_template("signup.html")

@app.route('/signup', methods=['POST'])
def signup():
    signinForm = request.form
    username = signinForm['username']
    password= signinForm['password']
    # todo validate 
    return render_template("room.html")

@app.route('/home')
def home():
    return "home page"